@extends('admin_layout.admin')

@section('title')
    Modifier Produit
@endsection

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Produit</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active">Produit</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Modifier Produit</h3>
                        </div>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach  
                                </ul>
                            </div>
                        @endif

                        @if (Session::has('status'))
                            <div class="alert alert-success">
                                {{ Session::get('status') }}
                            </div>
                        @endif

                        {!! Form::open(['action' => 'App\Http\Controllers\ProductController@updateproduct', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group">
                                {{ Form::hidden('id', $product->id) }}
                                {{ Form::label('', 'Nom produit', ['for' => 'exampleInputEmail1']) }}
                                {{ Form::text('product_name', $product->product_name, ['class' => 'form-control', 'id' => 'exampleInputEmail1', 'placeholder' => 'Enter product name']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('', 'Prix produit', ['for' => 'exampleInputEmail1']) }}
                                {{ Form::number('product_price', $product->product_price, ['class' => 'form-control', 'id' => 'exampleInputEmail1', 'placeholder' => 'Enter product price']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('', 'Catégorie produit') }}
                                {{ Form::select('product_category', $categories, $product->product_category, ['class' => 'form-control select2']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('', 'Description produit', ['for' => 'product_description']) }}
                                {{ Form::textarea('product_description', $product->product_description, ['class' => 'form-control', 'id' => 'product_description', 'placeholder' => 'Enter product description']) }}
                            </div>
                            <label for="exampleInputFile">Image Produit</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    {{ Form::file('product_image', ['class' => 'custom-file-input', 'id' => 'exampleInputFile']) }}
                                    {{ Form::label('', 'Choose file', ['class' => 'custom-file-label', 'for' => 'exampleInputFile']) }}
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            {{ Form::submit('Modifier', ['class' => 'btn btn-success']) }}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script src="{{ asset('backend/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script>
    $(function () {
        $.validator.setDefaults({
            submitHandler: function () {
                alert("Form successfully submitted!");
            }
        });
        $('#quickForm').validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 5
                },
                terms: {
                    required: true
                },
            },
            messages: {
                email: {
                    required: "Please enter an email address",
                    email: "Please enter a valid email address"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                terms: "Please accept our terms"
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
@endsection
