@extends('admin_layout.admin')

@section('title')
    Commandes
@endsection

@section('style')
<link rel="stylesheet" href="backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Orders</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Commandes</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Toutes les Commandes</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>Num. Commandes</th>
                          <th>Date</th>
                          <th>Nom client</th>
                          <th>Adresse</th>
                          <th>Commandes</th>
                          <th>Actions</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($orders as $order)
                      <tr>
                          <td>{{ $order->order_number }}</td>
                          <td>{{ $order->created_at }}</td>
                          <td>{{ $order->names }}</td>
                          <td>{{ $order->adresse }}</td>
                          <td>
                              @php
                                  $panier = unserialize($order->panier);
                              @endphp
                              <ul>
                                  @foreach ($panier->items as $item)
                                  <li>{{ $item['item']['product_name'] }} - Quantité: {{ $item['qty'] }}</li>
                                  @endforeach
                              </ul>
                          </td>
                          <td>
                              <a href="#" class="btn btn-primary"><i class="nav-icon fas fa-eye"></i></a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('scripts')

<!-- DataTables -->
<script src="backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="backend/dist/js/bootbox.min.js"></script>

<script>
$(document).on("click", "#delete", function(e){
e.preventDefault();
var link = $(this).attr("href");
bootbox.confirm("Do you really want to delete this element ?", function(confirmed){
  if (confirmed){
      window.location.href = link;
    };
  });
});
</script>
<!-- page script -->
<script>
$(function () {
  $("#example1").DataTable({
    "responsive": true,
    "autoWidth": false,
  });
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    "responsive": true,
  });
});
</script>
@endsection
