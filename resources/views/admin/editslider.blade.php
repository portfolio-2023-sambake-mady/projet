@extends('admin_layout.admin')

@section('title')
    Modifier Slider
@endsection

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Slider</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                        <li class="breadcrumb-item active">Slider</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Modifier slider</h3>
                        </div>
                        
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        {!! Form::open(['route' => 'updateslider', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group">
                                {{ Form::hidden('id', $slider->id)}}
                                {{ Form::label('description1', 'Description 1', ['for' => 'exampleInputEmail1']) }}
                                {{ Form::text('description1', $slider->description1, ['class' => 'form-control', 'id' => 'exampleInputEmail1', 'placeholder' => 'Enter description one']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description2', 'Description 2', ['for' => 'exampleInputEmail1']) }}
                                {{ Form::text('description2', $slider->description2, ['class' => 'form-control', 'id' => 'exampleInputEmail1', 'placeholder' => 'Enter description two']) }}
                            </div>
                            <label for="exampleInputFile">Slider image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    {{ Form::file('slider_image', ['class' => 'custom-file-input', 'id' => 'exampleInputFile']) }}
                                    {{ Form::label('Choose file', 'Choose file', ['class' => 'custom-file-label', 'for' => 'exampleInputFile']) }}
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            {{ Form::submit('Modifier', ['class' => 'btn btn-success']) }}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('scripts')
<script src="{{ asset('backend/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script>
$(function () {
    $.validator.setDefaults({
        submitHandler: function () {
            alert("Form successfully submitted!");
        }
    });
    $('#quickForm').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
                minlength: 5
            },
            terms: {
                required: true
            },
        },
        messages: {
            email: {
                required: "Please enter an email address",
                email: "Please enter a valid email address"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            terms: "Please accept our terms"
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});
</script>
@endsection
