<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

// */

require __DIR__.'/auth.php';

Route::get('/', [ClientController::class, 'home']);
Route::get('/shop', [ClientController::class, 'Shop']);
Route::get('/cart', [ClientController::class, 'Cart']);
Route::get('/paiement', [ClientController::class, 'Paiement']);
Route::get('/login', [ClientController::class, 'Login']);
Route::get('/logout', [ClientController::class, 'logout']);
Route::get('/signup', [ClientController::class, 'SignUp']);
Route::get('/orders', [ClientController::class, 'orders']);
Route::get('/ajouteraupanier/{id}', [ClientController::class, 'ajouteraupanier']);
Route::post('/modifier_quant/{id}', [ClientController::class, 'modifier_quant']);
Route::get('/suppdupanier/{id}', [ClientController::class, 'suppdupanier']);
Route::post('/creer_compte', [ClientController::class, 'creer_compte']);
Route::post('/acceder_compte', [ClientController::class, 'acceder_compte']);
Route::post('/payer', [ClientController::class, 'payer']);



Route::get('/admin', [AdminController::class, 'dashboard']);

Route::get('/addcategory', [CategoryController::class, 'AddCategory']);
Route::get('/categories', [CategoryController::class, 'categories']);
Route::post('/savecategory', [CategoryController::class, 'savecategory']);
Route::get('/edit_category/{id}', [CategoryController::class, 'edit_category']);
Route::post('/updatecategory', [CategoryController::class, 'updatecategory']);
Route::get('/delete_category/{id}', [CategoryController::class, 'delete_category']);

Route::get('/addslider', [SliderController::class, 'addslider'])->name('addslider');
Route::post('/saveslider', [SliderController::class, 'saveslider'])->name('saveslider');
Route::get('/sliders', [SliderController::class, 'sliders'])->name('sliders');
Route::get('/edit_slider/{id}', [SliderController::class, 'edit_slider'])->name('edit_slider');
Route::post('/updateslider', [SliderController::class, 'updateslider'])->name('updateslider');
Route::get('/delete_slider/{id}', [SliderController::class, 'delete_slider']);
Route::get('/desactiver_slider/{id}', [SliderController::class, 'desactiver_slider']);
Route::get('/activer_slider/{id}', [SliderController::class, 'activer_slider']);

Route::get('/addproduct', [ProductController::class, 'AddProduct'])->name('add_product');
Route::post('/saveproduct', [ProductController::class, 'saveproduct'])->name('save_product');
Route::get('/products', [ProductController::class, 'products'])->name('products');
Route::get('/edit_product/{id}', [ProductController::class, 'edit_product'])->name('edit_product');
Route::post('/updateproduct', [ProductController::class, 'updateproduct'])->name('update_product');
Route::get('/delete_product/{id}', [ProductController::class, 'delete_product'])->name('delete_product');
Route::get('/activer_product/{id}', [ProductController::class, 'activer_product'])->name('activer_product');
Route::get('/desactiver_product/{id}', [ProductController::class, 'desactiver_product'])->name('desactiver_product');
Route::get('/select_par_cat/{category_name}', [ProductController::class, 'select_par_cat']);

