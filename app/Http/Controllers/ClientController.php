<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use Session;
use App\Cart;
use Stripe\Charge;
use Stripe\Stripe;
use Illuminate\Support\Facades\Redirect;

class ClientController extends Controller
{
    public function home(){
        $sliders = Slider::all()->where('status', 1);
        $products = Product::all()->where('status', 1);

        return view('client.home')->with('sliders', $sliders)->with('products', $products);
    }

    public function shop(){
        $products = Product::all()->where('status', 1);
        $categories = Category::all();

        return view('client.shop')->with('products', $products)->with('categories', $categories);
    }

    public function ajouteraupanier($id){
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);
        Session::put('cart', $cart);

        return back();
    }

    public function cart()
    {
        if (!Session::has('cart')) {
            return view('client.cart');
        }

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $products = $cart->items;

        // Assurez-vous de récupérer la description pour chaque produit
        foreach ($products as &$product) {
            $productDetails = Product::find($product['product_id']);
            $product['product_description'] = $productDetails->product_description;
        }

        return view('client.cart', ['products' => $products]);
    }

    public function modifier_quant(Request $request, $id){
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->updateQty($id, $request->quantity);
        Session::put('cart', $cart);

        return back();
    }

    public function suppdupanier($id){
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        if(count($cart->items) > 0){
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }

        return back();
    }

    public function paiement(){
        if(!Session::has('client')){
            return view('client.login');
        }
        
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
    
        return view('client.paiement')->with('totalPrice', $cart->totalPrice)->with('items', $cart->items);
    }

    public function payer(Request $request){
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
    
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
    
        $token = $request->input('stripeToken');
    
        if (!$token) {
            return redirect('/paiement')->with('error', 'La validation du paiement a échoué. Veuillez réessayer.');
        }
    
        try {
            $charge = Charge::create([
                "amount" => $cart->totalPrice * 100,
                "currency" => "eur",
                "source" => $token,
                "description" => "Test Charge"
            ]);
        } catch (\Exception $e) {
            Session::put('error', $e->getMessage());
            return Redirect::to('/paiement');
        }
    
        $order = new Order();
        $order->names = $request->input('name');
        $order->adresse = $request->input('address');
        $order->panier = serialize($cart);
        $order->order_number = $this->generateOrderNumber(); // Génération du numéro de commande
        $order->save();
    
        Session::forget('cart');
    
        return redirect('/cart')->with('status', 'Votre commande a été effectuée avec succès !');
    }
    

    private function generateOrderNumber(){
        return strtoupper(uniqid('NUM-')); // Génère un numéro de commande unique
    }

    public function login(){
        return view('client.login');
    }

    public function logout(){
        Session::forget('client');
        return back();
    }

    public function signup(){
        return view('client.signup');
    }

    public function creer_compte(Request $request){
        $this->validate($request, [
            'email' => 'required|email|unique:clients',
            'password' => 'required|min:8'
        ]);

        $client = new Client();
        $client->email = $request->input('email');
        $client->password = bcrypt($request->input('password'));
        $client->save();

        return back()->with('status', 'Votre compte a été créé avec succès !');
    }

    public function acceder_compte(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $client = Client::where('email', $request->input('email'))->first();

        if ($client) {
            if (Hash::check($request->input('password'), $client->password)) {
                Session::put('client', $client); // Ajout du client dans la session
                return redirect('/shop');
            } else {
                return back()->with('status', 'Le mot de passe est incorrect');
            }
        } else {
            return back()->with('status', 'Nous n\'avons pas trouvé de compte associé à cet email, veuillez créer un compte');
        }
    }

    public function orders(){
        $orders = Order::all();
        $orders->transform(function($order, $key){
            $order->cart = unserialize($order->panier);
            return $order;
        });
        return view('admin.orders')->with('orders', $orders);
    }

    
}
